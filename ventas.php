<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html lang="es">
    <head>
        <?php include("codigos/meta.php")?>
        <title>ALLBUY-PRODUCTOS</title>
    </head>
    <body>
        <?php include("codigos/header.php")?>
        <?php include("codigos/nav.php")?>
        <!-- Product grid -->
        <div class="contenedorProductos">
                <table border="2">
                    
                      <?php include("modelo/getProductos.php");
                        
                        $arreglo_productos= obtener_productos();
                        for ($x=1;$x<=count($arreglo_productos); $x++){
                            if($x==1){
                                echo "<tr>";
                            }
                            echo "<td>"; 
                                echo "<div class='sensors' align='center'>";
                                    echo "<img src='imagenes/".$arreglo_productos[$x-1]->getPro_img()."' alt='".$arreglo_productos[$x-1]->getPro_nombre()."' style='width:200px;height:200px; align-content: center;'>";
                                    echo "<h4>".$arreglo_productos[$x-1]->getPro_nombre()."</h4>";
                                    echo "<p>".$arreglo_productos[$x-1]->getPro_descripcion()."<br><b>".$arreglo_productos[$x-1]->getPro_precio()."</b></p>";
                                echo "</div>";
                                echo "<div id='contenedor_anadir_cesta'>";
                                    echo "<form method='post' action='modelo/insertOrden.php' id='form_anadir_cesta'>";
                                        echo "<input type='hidden' name='idproducto' value='".$arreglo_productos[$x-1]->getProducto_id()."'>";
                                        echo "<input type='hidden' name='idusuario' value='".$usuario->getUsu_id()."'>";
                                        echo "<input type='hidden' name='precio' value='".$arreglo_productos[$x-1]->getPro_precio()."'>";
                                        echo "<label for='cantidad'>cantidad</label>";
                                        echo "<input type='number' id='b' name='cantidad' value='1' min='1' max='10'>";
                                        echo "<input type='submit' value='añadir a cesta' id='bt_submit'>";
                                    echo "</form>";
                                echo "</div>";
                            echo "</td>";
                            if(($x % 3 )==0 ){
                                echo "</tr>";
                                echo "<tr>";
                            }
                        }
                      ?>
                      <!--
                      <td>
                          <div class="sensors" align="center">
                            <img src="imagenes/sensordistancia.jpg" alt="ZX sensor de distancia y gesto" style="width:200px;height:200px; align-content: center;">
                            <h4>ZX sensor de distancia y gesto</h4>
                            <p>Reconocer la distancia de un objeto lejos del sensor hasta aproximadamente 12 pulgadas.<br><b>$24.95</b></p>
                          </div>
                          <div id="contenedor_anadir_cesta">
                              <form action="#" id="form_anadir_cesta" >
                                  <label for="b">cantidad</label>
                                  <input type="number" id="b" name="b" value="1" min="1" max="10"> 
                                  <input type="submit" value="añadir a cesta" id="bt_submit">
                            </form>
                          </div>
                      </td>
                      <td>
                          <div class="sensors" align="center">
                            <img src="imagenes/sensor1.jpg" style="width:200px;height:200px; align-content: center;">
                            <h4>IMU Breakout - MPU-9250 </h4>
                            <p>El Sparkfun MPU-9250 IMU Breakout cuenta con el último sensor MEMS 9 ejes de InvenSense.<br><b>$14.95</b></p>
                          </div>
                      </td>
                      <td>
                          <div class="sensors" align="center">
                            <img src="imagenes/shs.jpg" style="width:200px;height:200px; align-content: center;">
                            <h4>Sensor de Humedad de Suelo </h4>
                            <p>Sensor de Humedad de Suelo es fácil de usar, mide la humedad en el suelo y materiales 
                                similares.<br><b>$4.95</b></p>
                          </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                          <div class="sensors" align="center">
                            <img src="imagenes/sensor3.jpg" alt="ZX sensor de distancia y gesto" style="width:200px;height:200px; align-content: center;">
                            <h4>Sensor de radiación Geiger de bolsillo - Tipo 5</h4>
                            <p>Es un sensor de radiación de alta sensibilidad diseñado para el mercado de sistemas embebidos.<br><b>$24.95</b></p>
                          </div>
                      </td>
                      <td>
                          <div class="sensors" align="center">
                            <img src="imagenes/sensor4.jpg" style="width:200px;height:200px; align-content: center;">
                            <h4>Sensor de movimiento PIR (JST)</h4>
                            <p>Enciéndalo y espere 1-2 segundos para que el sensor obtenga una instantánea de la cámara. 
                            Si algo se mueve después de ese período, el pin de "alarma" bajará.<br><b>$9.95</b></p>
                          </div>
                      </td>
                      <td>
                          <div class="sensors" align="center">
                            <img src="imagenes/spulso.jpg" style="width:200px;height:200px; align-content: center;">
                            <h4>Sensor de Pulso</h4>
                            <p>El Sensor de Pulso Amped es un sensor de frecuencia cardíaca plug-and-play para Arduino.<br><b>$4.95</b></p>
                          </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                          <div class="shields" align="center">
                            <img src="imagenes/escudo1.jpg" style="width:200px;height:200px; align-content: center;">
                            <h4>Sparkfun GPS Logger Escudo</h4>
                            <p>El escudo se basa en un GP3906-TLP Módulo GPS - un receptor GPS de 66 canales que 
                                ofrece una arquitectura MT3339 de MediaTek y hasta una velocidad de actualización de 10 Hz.<br><b>$44.95</b></p>
                          </div>
                      </td>
                      <td>
                          <div class="shields" align="center">
                            <img src="imagenes/escudo2.jpg" style="width:200px;height:200px; align-content: center;">
                            <h4>Kit de Sparkfun shield joystick</h4>
                            <p>El escudo se encuentra en la parte superior de la placa Arduino y lo convierte en un controlador simple.<br><b>$12.95</b></p>
                          </div>
                      </td>
                      <td>
                          <div class="shields" align="center">
                            <img src="imagenes/escudo3.jpg" style="width:200px;height:200px; align-content: center;">
                            <h4>RS485 Escudo V3 - Frambuesa Pi</h4>
                            <p>El RS485 Escudo V3 es compatible con Frambuesa Pi B, B + y Frambuesa Pi 2.<br><b>$13.95</b></p>
                          </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                          <div class="shields" align="center">
                            <img src="imagenes/escudo4.jpg" style="width:200px;height:200px; align-content: center;">
                            <h4>Escudo MyoWare LED</h4>
                            <p>Con este escudo se le proporcionará con una representación visual de 
                                las señales proporcionadas por el sensor de Muscle MyoWare.<br><b>$24.95</b></p>
                          </div>
                      </td>
                      <td>
                          <div class="shields" align="center">
                            <img src="imagenes/escudo5.jpg" style="width:200px;height:200px; align-content: center;">
                            <h4>RFduino - Escudo USB</h4>
                            <p>Este es el escudo RFduino USB, una pequeña placa de complemento que se 
                                conecta a cualquier puerto USB y se utiliza para cargar sus bocetos en un módulo RFduino.<br><b>$25.95</b></p>
                          </div>
                      </td>
                      <td>
                          <div class="shields" align="center">
                            <img src="imagenes/escudo6.jpg" style="width:200px;height:200px; align-content: center;">
                            <h4>Sparkfun Fotón Weather Shield</h4>
                            <p>El Sparkfun Fotón El tiempo Shield es una tabla de complemento fácil 
                                de usar que le otorga acceso a la presión barométrica, humedad relativa y la temperatura.<br><b>$32.95</b></p>
                          </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                          <div class="boards" align="center">
                            <img src="imagenes/b2.jpg" style="width:200px;height:200px; align-content: center;">
                            <h4>SparkFun GS407 Breakout Board</h4>
                            <p>Este es un tablero del desbloqueo para permitir un acceso sencillo a los pasadores fuertemente inclinadas sobre las unidades GS406 y GS407 GPS.<br><b>$0.95</b></p>
                          </div>
                      </td>
                      <td>
                          <div class="boards" align="center">
                            <img src="imagenes/b3.jpg" style="width:200px;height:200px; align-content: center;">
                            <h4>LilyPad Vibe Board</h4>
                            <p>LilyPad es una tecnología portátil desarrollado por Leah Buechley y diseñada por Leah y Sparkfun.<br><b>$7.95</b></p>
                          </div>
                      </td>
                      <td>
                          <div class="boards" align="center">
                            <img src="imagenes/b4.jpg" style="width:200px;height:200px; align-content: center;">
                            <h4>Breakout Board for XBee Module</h4>
                            <p>Esta placa se desata los 20 pines del XBee a un 0,1" encabezado de fila estándar de espacio dual.<br><b>$2.95</b></p>
                          </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                          <div class="boards" align="center">
                            <img src="imagenes/b5.jpg" style="width:200px;height:200px; align-content: center;">
                            <h4>LilyPad Button Board</h4>
                            <p>CLilyPad es una tecnología de e-textiles portátil desarrollado por Leah Buechley y diseñada por Leah y Sparkfun.<br><b>$1.50</b></p>
                          </div>
                      </td>
                      <td>
                          <div class="boards" align="center">
                            <img src="imagenes/escudo5.jpg" style="width:200px;height:200px; align-content: center;">
                            <h4>micro:bit Board</h4>
                            <p>El micro BBC: bit es un ordenador de bolsillo que le permite ser creativo con la tecnología digital.<br><b>$14.95</b></p>
                          </div>
                      </td>
                      <td>
                          <div class="boards" align="center">
                            <img src="imagenes/b01.jpg" style="width:200px;height:200px; align-content: center;">
                            <h4>LilyPad Pixel Board</h4>
                            <p>La junta de píxeles es compatible con una WS2812B que es en realidad un RGB LED con una WS2811 integrada en el LED.<br><b>$3.95</b></p>
                          </div>
                      </td>
                    </tr>
                    -->
                </table>
            </div>
        <?php include("codigos/footer.php")?>
    </body>
</html>
